# Gitlab - Moodle automation 
Bachelor's final project.

This repository has different automation features. It can transfer your groups from
Moodle to Gitlab, transfer issues used as feedback for a project from Gitlab to moodle, 
and analyse participation of each member based on commits.

## Table of Contents

- [Installation](#installation)
- [Features](#features)
- [Usage](#usage)
- [Default](#default)
- [Documentation](#documentation)


## Requirements

Python 3.10.10 

Package manager for python: pip. 


## Installation

The only installation process needed is to clone this repository

```shell
$ git clone <repository_url>
$ cd moodlegitlabsync
$ pip install -r requirements.txt
```

## Features

This repository enables you to :

- Transfer existing groups from moodle to a gitlab parent group.
- Grade an individual moodle assignment from a feedback existing as an issue in gitlab, for every student in a course.
- Grade a group assignment in moodle from a feedback existing as an issue in gitlab, each student from the group
receives the same feedback.


## Usage
1) Group transfer.

```shell
$ python mgsync.py transf_groups config_file.json
```

Example config_file.json:
```json
{
    "course_id" : aNumber,
    "parent_group_id" : aNumber,
    "moodleTeacherToken" : "your_moodle_token",
    "gitPAToken" : "your_gitlab_personal_access_token",
    "gitDomain" : "your-domain-for-gitlab/",
    "moodleDomain": "your-domain-for-moodle/"
} 
```
- Assumptions : The gitlab API does not allow the creation of a main group, so there needs to be a manually created
parent group and the Moodle groups will be created in this parent group.
To be able to find the users in gitlab using the API, their email adress have to be specifically public otherwise it's not possible. 

2) Grade an individual project.

```shell
$ python mgsync.py grade_single config_file.json
```

Example config_file.json:
```json
 {
    "course_id" : aNumber,
    "assignmentNames": "oneAssignment",
    "moodleTeacherToken" : "your_moodle_token",
    "gitPAToken" : "your_gitlab_personal_access_token",
    "gitDomain" : "your-domain-for-gitlab/",
    "moodleDomain": "your-domain-for-moodle/"
}
```

- Assumptions : Same as before for public emails. On top of that, Gitlab projects and moodle assignments have to be 
named exactly the same. Otherwise, it is not possible to find and link correctly which
project goes with which assingment.

3) Grade a group project.

```shell
$ python mgsync.py grade_mul config_file.json
```


Example config_file.json:
```json
{
    "course_id" : aNumber,
    "assignmentNames": "oneAssignment",
    "parent_group_id": aNumber,
    "moodleTeacherToken" : "your_moodle_token",
    "gitPAToken" : "your_gitlab_personal_access_token",
    "gitDomain" : "your-domain-for-gitlab/",
    "moodleDomain": "your-domain-for-moodle/"
}
```

- Assumptions : Same as before for public emails. Same as before for project and assignment names.

4) Analyze participation.

```shell
$ python mgsync.py analyze_com config_file.json
```

Example config_file.json:

```json
{
    "project_name" : "myTestProject",
    "granularity" : "weeks",
    "gitPAToken" : "your_gitlab_personal_access_token",
    "gitDomain" : "your-domain-for-gitlab/"
}
```
- Assumptions : Same as before for public emails and the root folder of the repository needs to have an "output" folder 
to store the function result.
- Optional arguments : "parentGroupId" and "minParticipation". 
## Default

- gitDomain = https://renkulab.io/gitlab/
- moodleDomain = https://vlenptbgitlabmoodle.hevs.ch/

# Documentation 

The documentation of this project can be found in the folder "Documentation". 