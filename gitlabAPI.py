from datetime import datetime
import json
import requests
import re

renkuGit = 'renkulab.io'

gitDomain = ''

base_url = "https://" + gitDomain + "/gitlab/api/v4/"

group_base_url = base_url + "groups/"
proj_base_url = base_url + "projects/"

# when created needs to be saved otherWise lost token
# myToken2 = "glpat-_7ceGC7yM1N7aw267wDr"
myToken2 = ""

myHeader = {"PRIVATE-TOKEN": myToken2}

headers = {}

test1ProjId = "36089"

# parentGroupId = 70838
parentGroupId = 0
subGroupId = 70852

secondAccountId = 38112
poAccountID = 38111


# ------------------------------ FUNCTIONS FOR SYNC GROUPS CREATION ---------------------------------------------


def add_member_to_proj(user_id: int, proj_id: int) -> str:
    """
    This function adds a member to a project and returns the answer sent by server

    :param projID: Gitlab project to which the user will be added.
    :param user_id: Gitlab user ID that will be added to project.
    :return: Answer of the request to see result.
    """

    # user_id that we want to add to the project and his access lvl
    # data needs to be json format
    addData = {'user_id': str(user_id), 'access_level': '30'}
    proj_url = proj_base_url + str(proj_id) + '/members'

    # request to add a member
    response = requests.post(proj_url, data=addData, headers=headers)
    check_statusCode(proj_url, response)
    return response.text

def create_sub_group(subgroupe_name: str, path_name: str, parent_id: int) -> str:
    """
    This function creates a subgroup, in an existing parent group.
    Group Name can contain only letters, digits, '_', '-' and '.'. Cannot start with '-' or end in '.', '.git' or '.atom'.

    :param subgroupe_name: name of the subgroup that is created.
    :param path_name: path name in Gitlab, here same as "subgroupe_name".
    :param parent_id: initial group ID created via UI.
    :return: Answer of the request to see result.

    """

    addData = {'path': path_name, 'name': subgroupe_name, "parent_id": parent_id}
    group_url = group_base_url
    response = requests.post(group_url, data=addData, headers=headers)
    check_statusCode(group_url, response)

    # print(response)
    return response.text


def add_member_to_subgroup(user_id: int, group_id: int) -> None:  # -> str:
    """
    This function adds a member to a subgroup

    :param user_id: user ID that will be added to the group.
    :param group_id: group ID where user will be added.
    """

    # user_id that we want to add to the project and his access lvl
    # data needs to be json format
    addData = {'user_id': str(user_id), 'access_level': '30'}
    group_url = group_base_url + str(group_id) + '/members'
    # request to add a member
    # write_response(requests.post(group_url, data=addData, headers=headers).text, "gitLabInfo")
    response = requests.post(group_url, data=addData, headers=headers)
    check_statusCode(group_url, response)

    # to add return if we want to see pretty json
    # return requests.post(group_url, data=addData, headers=headers).text


def get_proj_members(proj_id: int) -> str:
    """
    This function returns a json file listing all members from a project.

    :param proj_id: project ID that information will be retrieved.
    :return: .json attribute of the answer to the request to see result.
    """

    proj_url = proj_base_url + str(proj_id) + "/members/all"
    # print(proj_url)

    response= requests.get(proj_url, headers=headers)
    check_statusCode(proj_url, response)
    return response.json()

def get_group_members(group_id: int) -> str:
    """
    This function returns all members from a group (inherited and invited members included)

    :param group_id: group ID that information will be retrieved.
    :return: .text attribute of the answer to the request to see result.
    """

    group_url = group_base_url + str(group_id) + "/members/all"

    response = requests.get(group_url, headers=headers)
    check_statusCode(group_url, response)
    return response.text


def get_subgroup_ID(parent_group_id: int, subGroupName: str) -> int:
    """
    This functions returns the ID of the subgroupName given in parameter if it exists else return 0.

    :param parent_group_id: Parent group ID containing the particular sub group.
    :param subGroupName: Name of the subgroup that needs existing confirmation.
    :return: ID number if group exist else 0.
    """
    group_url = group_base_url + str(parent_group_id) + "/subgroups"
    # print("get existing groups in parent group", group_url)
    response = requests.get(group_url, headers=headers)
    check_statusCode(group_url, response)
    response.list = json.loads(response.text)
    # print(response.list)

    index = 0

    for subGroupNum in response.list:
        if subGroupName == subGroupNum['name']:
            return subGroupNum['id']
        # else:
        # print("not same group Name")
    index += 1
    return 0


def get_userid_byUserEmail(userEmail: str) -> int:
    """
    This function returns the userid by giving the gitlab public userEmail as parameter
    if the userEmail isn't set to public then user can not be found.

    :param userEmail: Email of the user which ID will be retrieved.
    :return: userID to the corresponding Email if it can be found.
    """
    user_url = base_url + "users?search=" + userEmail
    # print("retrieve gitlab user email :", user_url)
    response = requests.get(user_url, headers=headers)
    check_statusCode(user_url, response)

    response.list = json.loads(response.text)
    if response.list:
        jsonObj = response.list[0]
        # print(jsonObj["id"])
        # print("PUBLIC EMAIL WAS FOUND")
        return jsonObj["id"]

        # return response.text and give to write_response() to see pretty json
        # user_url = base_url + "users/" + str(jsonObj["id"])
        # response = requests.get(user_url, headers=headers)
        # print(response.text)

    else:
        # print("ERROR : no public email for", userEmail)
        return 0
    # return response.text


def create_add_groups(groupAndUser: list, parent_group_id):
    """
    This function analyses the list given in parameter, it creates the group if not existent and adds corresponding
    users to the group created. If the group exists it just adds user.

    :param groupAndUser: list of dict containing [{'Group': 'groupName', 'userEmail': 'anEmail'},...].
    :param parent_group_id: Gitlab existing parent group ID where the new group needs to be created.
    """
    groupNumber = 0
    createdGroups = []
    myKey = 'userEmail'
    subGroupId = ''
    # WHEN INDEX = 0 ITS GROUP
    for dic in groupAndUser:
        index = 0

        for key in dic:
            if index == 0:
                # create the group on gitlab via api if it doesn't exist
                # print(get_subgroups(parent_group_id, dic[key]))
                if get_subgroup_ID(parent_group_id, dic[key]) == 0:
                    if (' ' in dic[key]):
                        dic[key] = dic[key].replace(" ", "_")

                    create_sub_group(dic[key], dic[key], parent_group_id)
                    createdGroups.append(dic[key])
                    sub_group_id = get_subgroup_ID(parent_group_id, dic[key])
                    print("Group created :", dic[key])
                else:
                    sub_group_id = get_subgroup_ID(parent_group_id, dic[key])
            # if subgroup exist or has just been created in parent group just add user
            else:
                userID = get_userid_byUserEmail(dic[key])
                if userID:
                    add_member_to_subgroup(userID, sub_group_id)
                    print("User added : ", dic[key])
                else:
                    print("No public email for", dic[key])

                groupNumber += 1
            index += 1


# ------------------------------ FUNCTIONS FOR SYNC GRADE & FEEDBACK ---------------------------------------------
def get_username_by_userID(userID: int) -> str:
    userName_url = base_url + "/users?id=" + str(userID)
    response = requests.get(userName_url, headers=headers)
    responseJSON = response.json()
    check_statusCode(userName_url, response)

    return responseJSON[0]['username']


def get_proj_name_by_id(proj_id: int) -> str:
    """
    This function returns the name of the project corresponding to the ID given in parameter.

    :param proj_id: ID number of the project which name is needed.
    :return: Name of the project with corresponding ID.
    """

    projName_url = base_url + "/projects/" + str(proj_id)

    response = requests.get(projName_url, headers=headers)
    responseJSON = response.json()
    check_statusCode(projName_url, response)

    result = responseJSON['name']
    return result


def get_user_projects(userID: int) -> list:
    """
    This function returns a list of dict with the public projects ID and name owned by the person corresponding to the
    ID given in parameter.

    :param userID: Gitlab user ID number of which the public projects will be shown.
    :return: List of dict with : [{ projectID : projectName}, ... ]
    """

    myProj_url = base_url + "users/" + str(userID) + "/projects"

    response = requests.get(myProj_url, headers=headers)
    responseJSON = response.json()
    check_statusCode(myProj_url, response)

    result = []

    for projects in responseJSON:
        result.append({projects['id']: projects['name']})

    # print(result)
    return result


def get_issue_from_student(studentID: int, projectIds: list, group: bool) -> list:
    """
    This function retrieves all the issues named "Feedback grades" for the student id given in parameter, filtered by
    concerning the project IDs given in parameter.
    
    :param studentID: Gitlab user ID corresponding to the student.
    :param projectIds: List with project IDs which issue will be retrieved
    :param group: boolean used to differentiate if need of issue of each student, or a project issue for a group assignment.
    :return: List of dict with [{"project ID" : aNumber},{"Grade" : sum of all points}, {"Comment" : "question number = the comment"}, ...]
    """

    result = []
    allPoints = []
    gradeList = []
    allComments = []
    tempComments = []
    questionList = []
    issueInfo = []

    theProjId = re.findall(r'\d+', str(projectIds[0].keys()))
    # print(theProjId)

    if group:
        issue_url = base_url + "projects/" + theProjId[0] + "/issues"

    else:
        issue_url = base_url + "issues?assignee_id=" + str(studentID)

    # print("retrieve gitlab issue url :", issue_url)

    response = requests.get(issue_url, headers=headers)
    responseJSON = response.json()
    check_statusCode(issue_url, response)

    # print("resp", response)
    if not responseJSON:
        print("no match for for this student :", get_username_by_userID(studentID))
        return

    # iterate through all the issues concerning the user
    for issue in responseJSON:
        # iterate through the projects to which the user is linked
        for myProjId in projectIds:
            # print(myProjId.keys())
            # filtering incorrect issues
            if myProjId.keys().__contains__(issue['project_id']) and issue['title'] == "Feedback grades":
                issueInfo.append(issue['description'])
                break
            else:
            # can be either not a match between project and assignment
            # or can be no correctly named issue or no issue at all
                print("no issue for this student :", get_username_by_userID(studentID))

    # print(issueInfo)
    """if not issueInfo:
        print("STUDENT :", response[0]['assignees'][0]['name'], "DOES NOT HAVE AN ISSUE FOR THIS PROJECT")
        return"""
    # analyse one issue at a time
    for anIssue in issueInfo:
        if not group:
            if studentID != responseJSON[0]['assignees'][0]['id']:
                print("next issue")
                break
        # gathering all the grades and making the sum of them all
        allPoints.append(re.findall("\d+\.\d+", anIssue))
        for numbers in allPoints:
            tempRes = 0
            for aGrade in numbers:
                tempRes += float(aGrade)
        gradeList.append(tempRes)

    # gather the question number and its comments in the issue and put in format to pass
    # to the function sending api method
    # (done by string manipulation)
    correspondingIssue = 0
    for oneIssue in issueInfo:
        tempComments = oneIssue.split("Comments: ")
        tempComments.pop(0)

        index = 1
        tempQuestions = oneIssue.split("**")
        for aComment in tempComments:
            # print(aComment)
            allComments.append({correspondingIssue: aComment.split("\n")[0]})
        correspondingIssue += 1
        # print(allComments)

        for aQuestion in tempQuestions:
            # print(aQuestion)
            # we need all question numbers and they're positioned at even index numbers
            if index % 2 != 0 and index < len(tempQuestions):
                questionList.append(tempQuestions[index])
            index += 1

    # print(questionList)

    questionNumber = ""

    issueNumber = 0
    oldIssueNumber = 0

    # putting in format the result that we are giving back
    # -> [{"project ID" : ID1}, {"Grade" : grade of 1st project}, {"Comment :", "question a = comment"}]

    for projectofTheIssue in projectIds:
        actualIssueNumber = re.findall(r'\d+', str(projectofTheIssue.keys()))
        if oldIssueNumber != actualIssueNumber:
            # print("changing project : ", result)
            theID = re.findall(r'\d+', str(projectofTheIssue.keys()))
            result.append({"project ID": theID})
            # break

        # normally always one grade per issue so at first place
        if group:
            result.append({"Grade ": str(gradeList[0])})
        else:
            result.append({"Grade ": str(gradeList[0])})

        for comments in allComments:
            # print(comments.keys().__contains__(issueNumber))
            # issue comments are all in the same list
            # condition identifies when are at another project that the on concerning issueNumber
            # if comments.keys().__contains__(issueNumber) != 1:
            # print("not appropriate key")

            if comments.keys().__contains__(issueNumber) == 1 and comments[issueNumber] != 'nan':
                commentNumber = allComments.index(comments)
                questionNumber = questionList[commentNumber]
                result.append(
                    {"Comment": "question " + questionNumber + ' = ' + allComments[commentNumber][issueNumber]})

            # else:
            # print("no comment")
        issueNumber += 1

    # print(result)
    return result


def get_group_projects(subGroupId: int, projectName: str):
    """
    This function returns the project corresponding to the name present in a subgroup.
    
    :param subGroupId: Gitlab existing subgroup ID.
    :param groupName: Gitlab existing project name which  will be retrieved.
    :return: First project listed in subgroup {projectID : projectName}
    """

    result = []

    group_url = base_url + "groups/" + str(subGroupId) + "/projects"

    response = requests.get(group_url, headers=headers)
    responseJSON = response.json()
    check_statusCode(group_url, response)

    for aProject in responseJSON:
        if aProject['name'] == projectName:
            result = {aProject['id']: aProject['name']}

    # print(result)
    return result


def get_fork_proj_ID(base_proj_ID: int, corresponding_user: int) -> list:
    """
       This function returns a list of all forked project from the corresponding parent project ID given in parameter.

       :param base_proj_ID: Gitlab existing base project that student fork to have his own project.
       :return: List of all forked proj ID.
       """
    result = []
    fork_url = proj_base_url + str(base_proj_ID) + "/forks"

    response = requests.get(fork_url, headers=headers)
    responseJSON = response.json()
    check_statusCode(fork_url, response)

    for aProj in responseJSON:
        if aProj['owner']['id'] == corresponding_user:
            result.append({aProj['id']: aProj['name']})
    return result


# ------------------------------ FUNCTIONS FOR COMMITS ANALYSIS ---------------------------------------------


def get_commits_from_project(projID: int) -> list:
    """
    This function returns two results, one is a list of all commits made in a project, the other one is the dates of
    the beginning and the end of the project.

    :param projID: Gitlab project ID which commits will be analysed.
    :returns result: List of dict with {"author": commitAuthor, "author_email": 'author_email',"Title": commitTitle], "Date": commitDate, "Additions": number of line added for this commit}.
    :return beginEndDates: [beginDate, endDate].
    """
    result = []
    page_number = 1
    notFinalPage = True
    totalCommits = 0
    beginDate = ""
    endDate = ""
    currentDateFormat = "%Y-%m-%dT%H:%M:%S.%f%z"
    wantedDateFormat = "%d-%m-%YT%H:%M:%S.%f%z"
    beginEndDates = []
    while notFinalPage:

        commits_url = proj_base_url + str(projID) + "/repository/commits?with_stats=True&per_page=10000&page=" + str(
            page_number)

        response = requests.get(commits_url, headers=headers)
        responseJSON = response.json()
        check_statusCode(commits_url, response)

        # Security check
        # print(response)
        if (not issubclass(type(responseJSON),list)) and "403" in responseJSON['message']:
            print("Not correct rights to query project with ID :", projID)
            return 0,0
        # print(commits_url)
        if totalCommits == 0:
            # print("first commit")
            endDate = responseJSON[0]['committed_date']

        # print(response)
        if responseJSON:
            # print("page not empty")
            for aCommit in responseJSON:
                theDateobject = datetime.strptime(aCommit['committed_date'], currentDateFormat)
                theOutputDate = theDateobject.strftime(wantedDateFormat)
                # print(aCommit)
                result.append({"author": aCommit['author_name'], "author_email": aCommit['author_email'],
                               "Title": aCommit['title'], "Date": theDateobject,
                               "Additions": aCommit['stats']['additions']})
                # print(result)
                totalCommits += 1
            # keep last date in memory
            beginDate = responseJSON[len(responseJSON) - 1]['committed_date']
        else:
            notFinalPage = False

        page_number += 1

        # print("Page num = ", page_number)

    beginEndDates.append(beginDate)
    beginEndDates.append(endDate)

    return result, beginEndDates


def get_all_subgroup(parentGroupID: int):
    """
    This function returns all the IDs of the subgroups present in parent group.

    :param parentGroupID: Parent group ID number containing all subgroups.
    :return: List with all subgroup ID
    """
    group_url = group_base_url + str(parentGroupID) + "/subgroups"
    print("get existing groups in parent group", group_url)

    response = requests.get(group_url, headers=headers)
    check_statusCode(group_url, response)

    response.list = json.loads(response.text)
    # print(response.list)

    result = []

    for oneGroup in response.list:
        result.append({oneGroup['id']: oneGroup['name']})

    return result


def get_project_ID_in_subGroup(subgroupID: int, projectName: str):
    """
    This function returns the Id of the project if the name in parameter corresponds.
    If the project name is not found it returns 0.

    :param parentGroupID: Parent group ID number containing all subgroups.
    :return: List with all subgroup ID
    """
    group_url = base_url + "groups/" + str(subgroupID) + "/projects"
    response = requests.get(group_url, headers=headers)
    responseJSON = response.json()
    check_statusCode(group_url, response)
    result = 0
    for oneProject in responseJSON:
        if oneProject['name'] == projectName:
            result = oneProject['id']

    return result


def get_all_project_IDs_matching_name(projectName: str) -> list:
    """
    This function returns a list of all the project found that matches the name given in parameter.

    :param projectName: Name of the projects which IDs are needed.
    :return: List with all project IDs
    """
    proj_url = base_url + "projects?search=" + projectName + "&per_page=10000"
    # print(proj_url)

    response = requests.get(proj_url, headers=headers)
    check_statusCode(proj_url, response)
    responseJSON = response.json()

    result = []
    # print(response)
    for oneProject in responseJSON:
        if oneProject['name'] == projectName and len(oneProject['name']) == len(projectName):
            result.append(oneProject['id'])
    print("nummber of projects found matching the name :", len(result))
    return result



def check_statusCode(urlUsed: str, answer : requests.Response):
    """
        This function prints the answer message in case an error code is answered because of an error response from
        client (400-499) or from server (500-599).

        :param urlUsed: The url used for the http method.
        :param answerCode: The response code from the method made.
        :return: True if no error, False if error.
    """
    if 400 <= answer.status_code <= 599:
        print("Error message :", answer.json(), "\nURL :", urlUsed)
    elif 200 <= answer.status_code <= 299:
        "Status code is ok"  # Because status code ok
    else:
        print("Unkown code :", answer.status_code, "\nURL :", urlUsed)

