<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block definition class for the block_grades_feedback plugin.
 *
 * @package   block_grades_feedback
 * @copyright Year, You Name <your@email.address>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class block_grades_feedback extends block_base {

  
    /**
     * Initialises the block.
     *
     * @return void
     */
    public function init() {
        $this->title = get_string('pluginname', 'block_grades_feedback');
        $this->config = new stdClass();
    }

    

    /**
     * Gets the block contents.
     *
     * @return string The block HTML.
     */
    public function get_content() {
        global $OUTPUT;

        if ($this->content !== null) {
            return $this->content;
        }

        $this->content = new stdClass();
        $this->content->footer = '';

        // Add logic here to define your template data or any other content.
        $data = ['content' => '
        <div>
            <h1>Welcome to Your Plugin</h1>
            <p>This is the content of your grade and feedback block.</p>
        </div>
    '];

        //$this->content->text = $OUTPUT->render_from_template('block_yourplugin/content', $data);
        $this->content->text = '
        <div>
            <p>Add a configuration file:</p>
            <p><a href="#" id="singleProject">Grade Single Project!</a></p>
            <p><a href="#" id="groupProject">Grade Group Project!</a></p>
        </div>
        <div id="gradeFeedbackConfig">
            <p>Drag and drop a file here</p>
            <input type="file" id="fileInputGF" style="display: none;" onchange="handleFileChangeGF()" />
            <button onclick="openFileInputGF()">Choose a File :</button>
        </div>
        <div id="selectedFileNameGF"></div>


        <script type="text/javascript">

            function openFileInputGF() {
                // Trigger the click event of the hidden file input element
                document.getElementById("fileInputGF").click();
            }


            function handleFileChangeGF() {
                var fileInput = document.getElementById("fileInputGF");
                var selectedFileNameGF = document.getElementById("selectedFileNameGF");
                // Check if a file was selected
                if (fileInput.files.length > 0) {
                    // Get the first selected file
                    var file = fileInput.files[0];
                    // Display the file name on the div
                    selectedFileNameGF.textContent = "Selected File: " + file.name;
                } else {
                    // If no file is selected, clear the div content
                    selectedFileNameGF.textContent = "ciao";
                }
            }

            document.addEventListener("DOMContentLoaded", function() {
                var singleProj = document.getElementById("singleProject");
                var groupProj = document.getElementById("groupProject");
                var GFConfig = document.getElementById("gradeFeedbackConfig");
                var fileInput = document.getElementById("fileInputGF");

                singleProj.addEventListener("click", function(e) {
                    e.preventDefault();
                    sendAjaxRequest(0);
                });

                groupProj.addEventListener("click", function(e) {
                    e.preventDefault();
                    sendAjaxRequest(1);
                });

                GFConfig.addEventListener("dragenter", function(e) {
                    e.preventDefault();
                    GFConfig.classList.add("dragover");
                });

                GFConfig.addEventListener("dragleave", function(e) {
                    e.preventDefault();
                    GFConfig.classList.remove("dragover");
                });

                GFConfig.addEventListener("dragover", function(e) {
                    e.preventDefault();
                });

                GFConfig.addEventListener("drop", function(e) {
                    e.preventDefault();
                    GFConfig.classList.remove("dragover");

                    var file = e.dataTransfer.files[0];
                    dropmsg(file);
                });

                fileInput.addEventListener("change", function() {
                    var file = fileInput.files[0];
                    dropmsg(file);
                });

                function dropmsg(file) {
                    // Handle the dropped file, if needed (e.g., show a preview)
                    console.log("Grade FeedbackDropped file:", file.name);
                }

                function sendAjaxRequest(mode) {
                    var formData = new FormData();
                    formData.append("file", fileInput.files[0]);

                    var xhr = new XMLHttpRequest();
                    if (mode===0){
                        xhr.open("POST", "../blocks/grades_feedback/python/gradeSingleProject.php", true);
                    }
                    else {
                        xhr.open("POST", "../blocks/grades_feedback/python/gradeGroupProject.php", true);
                    }
                    xhr.onreadystatechange = function() {
                        if (xhr.readyState === XMLHttpRequest.DONE) {
                            if (xhr.status === 200) {
                                var result = xhr.responseText;
                                if (result.includes("Error")){
                                    alert("request correct but " + result)
                                }
                                else {
                                    console.log("Hello, World! Result from PHP function: " + result);
                                }
                            } else {
                                alert("Error: Unable to execute PHP function. Error:" + xhr.status);
                               
                            }
                        }
                    };
                    xhr.send(formData);
                }
            });
        </script>

    ';
        return $this->content;
    }

    /**
     * Defines in which pages this block can be added.
     *
     * @return array of the pages where the block can be added.
     */
    public function applicable_formats() {
        return [
            'admin' => false,
            'site-index' => true,
            'course-view' => true,
            'mod' => false,
            'my' => true,
        ];
    }
}