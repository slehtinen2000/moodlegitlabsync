<?php
function python($filePath) {

    // Replace "config_file.json" with the actual file path received as an argument
    echo shell_exec("python3 main.py GM " . $filePath);
}

// Check if a file was uploaded via the drag and drop area
if (isset($_FILES['file']) && $_FILES['file']['error'] === UPLOAD_ERR_OK) {
    $uploadedFilePath = $_FILES['file']['tmp_name'];
    python($uploadedFilePath);
} else {
    // If no file was uploaded, you can handle the error or provide an appropriate response.
    echo "Error: No file was uploaded.";
}
?>