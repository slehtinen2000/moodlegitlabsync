import json
import gitlabAPI
import requests

hesMoodle = 'vlenptbgitlabmoodle.hevs.ch'
moodleDomain = ''

base_url = "https://" + moodleDomain + "/webservice/rest/server.php?"

moodleAdminToken = "8fc126305723a0493155e9ebd440e5b7"

# moodleTeacherToken = "27af32036596b3f3a63512a4a7084d42"

moodleTeacherToken = ""

# usedToken = moodleTeacherToken

usedToken = ""


# ------------------------------ FUNCTIONS FOR SYNC GROUPS CREATION ---------------------------------------------


def get_course() -> str:
    """
        This function gives the info about whole site.

        :return: .text attribute of the request to see result.
    """
    group_url = base_url + 'wstoken=' + usedToken + '&wsfunction=core_course_get_courses&moodlewsrestformat=json'
    # print("URl used : ", group_url)
    response = requests.post(group_url)
    return response.text


# SEARCH DIFFERENCE BETWEEN COURSE MODULE ID AND COURSEID
def get_user_in_course(courseID: int) -> str:
    """
    This function returns all users from a course.

    :param courseID: Moodle course ID.
    :return: Json format of the answer with list of user enrolled in course.
    """

    group_url = base_url + 'wstoken=' + usedToken + '&wsfunction=core_enrol_get_enrolled_users&courseid=' + str(
        courseID) + '&moodlewsrestformat=json'
    # print("URl used : ", group_url)
    response = requests.post(group_url).json()
    return response



def get_group_members_by_id(group_id: list[int]) -> list[int]:
    """
    This function returns a list of group ids and the user ids that are present in the group.

    :param group_id: List of moodle groups to which user IDs  will be retrieved.
    :return: List of dict with ["groupID": groupIDnumber, "userID": userIDnumber, ...]
    """
    index = 0
    group_url = base_url + 'wstoken=' + usedToken + '&wsfunction=core_group_get_group_members'

    for numberofGroup in group_id:
        # print(str(numberofGroup))
        # for individualGroup in numberofGroup:
        group_url += '&groupids[' + str(index) + ']=' + str(numberofGroup)
        index += 1

    group_url += '&moodlewsrestformat=json'

    # print("get group members : ", group_url)
    response = requests.post(group_url)
    result = []
    # print(response.text)
    # return response.text
    for groupInfo in response.json():
        result.append({"groupID": groupInfo["groupid"], "userID": groupInfo["userids"]})
        # print('result : ', result)
    return result


def get_token_by_username():
    """
    This function prints out the token of the user in parameters.
    """
    url = base_url + 'username=user&password=bitnami&service=WRA'
    print("URl used : ", url)
    response = requests.get(url)
    print(response.text)


def get_user_email_from_id(user_ids: dict, groupTransfert : bool):
    """
    This function returns a list of dict containing moodle user information.

    :param user_ids: dict of user info with a case named ['userID'].
    :param groupTransfert: bollean used to differentiate if this function is used to transfer groups or not.
    :return: json string with the user email information.
    """
    # print(user_ids)
    index = 0
    response = ""
    user_id_url = base_url + 'wstoken=' + usedToken + '&wsfunction=core_user_get_users_by_field&field=id'
    for oneGroup in user_ids:

        for numberofUsers in oneGroup['userID']:
            user_id_url += '&values[' + str(index) + ']=' + str(numberofUsers)
            index += 1

    user_id_url += '&moodlewsrestformat=json'
    # print("get user email : ", user_id_url)

    response = requests.get(user_id_url)

    tempRes = response.json()
    if groupTransfert :
        index = 0
        # once we have list of students
        for oneStudent in tempRes:
            # we match them in their respective groups
            for oneGroup in user_ids:

                for studentID in oneGroup['userID']:
                    # if the students Id of the group matches we add it to the json
                    if tempRes[index]['id'] == studentID:
                        oneStudent['groupID'] = oneGroup['groupID']
            index += 1

    result = json.dumps(tempRes)
    #   give this to write response to see pretty json
    # print(result)
    return str(result)


def get_group_name_by_courseid(courseID: int) -> dict:
    """
    This function the group names and their ID in the course given in parameter.

    :param courseID: Moodle course to which the group information will be retrieved.
    :return: List of dict with [{"groupName": aGroupName, "groupID": groupIDnumber}, ...]
    """

    group_name_url = base_url + 'wstoken=' + usedToken + '&wsfunction=core_group_get_course_groups&courseid=' + str(
        courseID) + "&moodlewsrestformat=json"
    # print("get group name : ", group_name_url)
    response = requests.get(group_name_url)
    jsonResp = json.loads(response.text)
    result = []
    for groupInfo in jsonResp:
        # print(groupInfo['name'])
        result.append({"groupName": groupInfo['name'], "groupID": groupInfo['id']})

    return result


# ------------------------------ FUNCTIONS FOR SYNC GRADE & FEEDBACK ---------------------------------------------

def get_user_id_from_email(userEmail: str) -> int:
    """
    This function returns the user ID for the corresponding email given in parameter.

    :param userEmail: Moodle user email.
    :return: Moodle user ID number.
    """

    userID_url = base_url + 'wstoken=' + usedToken + "&wsfunction=core_user_get_users_by_field&field=email&values[0]=" + userEmail + "&moodlewsrestformat=json"
    response = requests.get(userID_url).json()
    if response:
        return response[0]['id']
    else:
        print("Error : user", userEmail, "not found")



def get_course_assignement_ID(courseID: int, assignementName: str) -> int:
    """
    This function retrieves a moodle assignment ID corresponding to the assignmentName parameter in the given courseID parameter.

    :param courseID: Moodle course ID number.
    :param assignementName: Moodle assignment name.
    :return: Moodle assignement ID number.
    """
    assignement_url = base_url + 'wstoken=' + usedToken + "&wsfunction=mod_assign_get_assignments&courseids[0]="+str(courseID)+"&moodlewsrestformat=json"
    # print(assignement_url)
    response = requests.get(assignement_url).json()
    result = []

    for dico in response['courses']:
        # print(response['courses'])
        # print(dico)
        # print(assignementName[0])
        for theName in dico['assignments']:
            if theName['name'] == assignementName:
                print("Moodle assignment", theName['name'], "found")
                result.append({theName['id']: theName['name']})
                # print("FOUND THE ASSIGNMENT")

    # print(result)
    return result


def set_assignement_grade_feedback(assignmentid: list, userID: int, addattempt: int, workflowState: str,
                                   applytoall: int,
                                   feedback: list) -> str:
    """
    This function retrieves issue description from gitlab and grades the assignment in moodle if found "Comment" and "points" in gitlab issue.

    :param assignmentid: parameter description.
    :param userID: parameter description.
    :param addattempt: parameter description.
    :param workflowState: parameter description.
    :param applytoall: parameter description.
    :param feedback: parameter description.
    :return: null if worked correctly.

    """

    grade = ""
    feedback_comment = ""
    # print(feedback[0].keys())

    currentProj = feedback[0]['project ID']
    oldProj = feedback[0]["project ID"]

    myIterator = iter(feedback)

    list_len = len(feedback)

    actualProj = currentProj[0]
    index = 0
    # iterate through list of dict containing info on each issue from gitlab projects for the user
    # print(feedback)
    for dico in feedback:

        # python way that I found to access the value of the dict at the current list object position
        for dicoVal in dico:
            # concatenate feedback comments
            if dicoVal == "Comment":
                feedback_comment += dico[dicoVal] + "\n"

            # identify wich project issue is in question
            elif dicoVal == "project ID":
                currentProj = dico[dicoVal]

            else:
                grade = dico[dicoVal]
            # print("current :",currentProj)
            # print("old :",oldProj)
            # Identify when we talk about another project so we can execute the post mehtod to update moodle assignment
            if currentProj != oldProj or index == list_len - 1:
                projName = gitlabAPI.get_proj_name_by_id(actualProj)
                assignID = str(get_key(projName, assignmentid))
                actualProj = currentProj[0]
                assignment_subStatus_url = base_url + "wstoken=" + usedToken + "&wsfunction=mod_assign_save_grade&assignmentid=" + assignID + "&userid=" + str(
                    userID) + "&grade=" + grade + "&attemptnumber=0" + "&addattempt=" + str(
                    addattempt) + "&workflowstate=" + workflowState + "&applytoall=" + str(
                    applytoall) + "&plugindata[assignfeedbackcomments_editor][text]=" + feedback_comment + "&plugindata[assignfeedbackcomments_editor][format]=" + str(
                    0) + "&plugindata[files_filemanager]=" + str(0) + "&moodlewsrestformat=json"
                # print("set assignment submission status url :", assignment_subStatus_url)
                userEmail = json.loads(get_user_email_from_id([{'userID': [userID]}], False))
                print("user graded :", userEmail[0]['email'])
                response = requests.post(assignment_subStatus_url)
                # print(response)
                oldProj = dico[dicoVal]
                feedback_comment = ""

        index += 1

    return response.text


"""this function identifies the student submission for the corresponding moodle assignments"""
def get_submission_id(assignementID: int) -> int:
    submission_state_url = base_url + 'wstoken=' + usedToken + "&wsfunction=mod_assign_get_submissions&assignmentids[0]=" + str(
        assignementID) + "&moodlewsrestformat=json"
    print("get submission id url :", submission_state_url)
    response = requests.get(submission_state_url).json()
    # return response.text
    result = response['assignments'][0]['submissions'][0]['id']
    return result


"""this function is used to retrieve the key from the dict without precising the value"""
def get_key(val, adico):
    for oneAssignment in adico:
        for key, value in oneAssignment.items():
            if val == value:
                return key

    return "key doesn't exist"
