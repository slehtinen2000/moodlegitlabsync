<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block definition class for the block_group_transfer plugin.
 *
 * @package   block_group_transfer
 * @copyright Year, You Name <your@email.address>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class block_group_transfer extends block_base {

  
    /**
     * Initialises the block.
     *
     * @return void
     */
    public function init() {
        $this->title = get_string('pluginname', 'block_group_transfer');
        $this->config = new stdClass();
    }

    

    /**
     * Gets the block contents.
     *
     * @return string The block HTML.
     */
    public function get_content() {
        global $OUTPUT;

        if ($this->content !== null) {
            return $this->content;
        }

        $this->content = new stdClass();
        $this->content->footer = '';

        // Add logic here to define your template data or any other content.
        $data = ['content' => '
        <div>
            <h1>Welcome to Your Plugin</h1>
            <p>This is the content of your group transfer block.</p>
        </div>
    '];

        //$this->content->text = $OUTPUT->render_from_template('block_yourplugin/content', $data);
        $this->content->text = '
        <div>
            <p>Add a configuration file:</p>
            <p><a href="#" id="myLink">Click Me!</a></p>
        </div>
        <div id="drag-drop-area">
            <p>Drag and drop a file here</p>
            <input type="file" id="fileInput" style="display: none;" onchange="handleFileChange()" />
            <button onclick="openFileInput()">Choose a File :</button>
        </div>
        <div id="selectedFileName"></div>


        <script type="text/javascript">

            function openFileInput() {
                // Trigger the click event of the hidden file input element
                document.getElementById("fileInput").click();
            }


            function handleFileChange() {
                var fileInput = document.getElementById("fileInput");
                var selectedFileName = document.getElementById("selectedFileName");
                // Check if a file was selected
                if (fileInput.files.length > 0) {
                    // Get the first selected file
                    var file = fileInput.files[0];
                    // Display the file name on the div
                    selectedFileName.textContent = "Selected File: " + file.name;
                } else {
                    // If no file is selected, clear the div content
                    selectedFileName.textContent = "ciao";
                }
            }

            document.addEventListener("DOMContentLoaded", function() {
                var link = document.getElementById("myLink");
                var dragDropArea = document.getElementById("drag-drop-area");
                var fileInput = document.getElementById("fileInput");

                link.addEventListener("click", function(e) {
                    e.preventDefault();
                    sendAjaxRequest();
                });

                dragDropArea.addEventListener("dragenter", function(e) {
                    e.preventDefault();
                    dragDropArea.classList.add("dragover");
                });

                dragDropArea.addEventListener("dragleave", function(e) {
                    e.preventDefault();
                    dragDropArea.classList.remove("dragover");
                });

                dragDropArea.addEventListener("dragover", function(e) {
                    e.preventDefault();
                });

                dragDropArea.addEventListener("drop", function(e) {
                    e.preventDefault();
                    dragDropArea.classList.remove("dragover");

                    var file = e.dataTransfer.files[0];
                    handleDroppedFile(file);
                });

                fileInput.addEventListener("change", function() {
                    var file = fileInput.files[0];
                    handleDroppedFile(file);
                });

                function handleDroppedFile(file) {
                    // Handle the dropped file, if needed (e.g., show a preview)
                    console.log("Dropped file:", file.name);
                }

                function sendAjaxRequest() {
                    var formData = new FormData();
                    formData.append("file", fileInput.files[0]);

                    var xhr = new XMLHttpRequest();
                    xhr.open("POST", "../blocks/group_transfer/python/pythonAccess.php", true);
                    xhr.onreadystatechange = function() {
                        if (xhr.readyState === XMLHttpRequest.DONE) {
                            if (xhr.status === 200) {
                                var result = xhr.responseText;
                                console.log("Hello, World! Result from PHP function: " + result);
                                //alert("SUCCESS !")
                            } else {
                                alert("Error: Unable to execute PHP function. Error:" + xhr.status);
                            }
                        }
                    };
                    xhr.send(formData);
                }
            });
        </script>

    ';
        return $this->content;
    }

    /**
     * Defines in which pages this block can be added.
     *
     * @return array of the pages where the block can be added.
     */
    public function applicable_formats() {
        return [
            'admin' => false,
            'all' => true,
            'site-index' => true,
            'course-view' => true,
            'mod' => false,
            'my' => true,
        ];
    }
}