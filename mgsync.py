import json
import subprocess
import webbrowser

import gitlabAPI
import sys
import argparse
import matplotlib.pyplot as plt
import datetime
import moodleAPI
import math
from datetime import timedelta


def write_response_json(response: str, fileName: str):
    """
    This function writes a formatted json into a file that can be used to see server's answer more clearly.
    :param response: The .text attribute of the http answer.
    :param user_id: The file path to write the answer.
    """

    json_object = json.loads(response)

    json_formatted_str = json.dumps(json_object, indent=4)
    # a for append / add to the file
    # w for overWrite
    f = open(fileName + ".json", "w")
    f.write(json_formatted_str)
    f.close()


def JSON_parse_email_moodle(members_in_group, groupnames: list, fileName: str) -> list:
    """
    this function reads the JSON file to find userEmail and pair the userEmail with the correct group according to
    matches between userID and groupID in "members_in_group".

    :param members_in_group: List of dict with [{'groupID' : number, 'userID' : [list of all user ids]}, ...].
    :param groupnames: List of dict with [{"groupName": groupInfo['name'], "groupID": groupInfo['id']},...].
    :param groupnames: The file path to be parsed.
    :return: list of dict with [{'Group': 'groupName', 'userEmail': 'anEmail'},...].
    """
    with open(fileName + ".json", 'r') as myjsonFile:
        manipJson = json.loads(myjsonFile.read())

    group_User = []
    index = 0
    # extra loop to delay index incrementation
    for groupNumber in members_in_group:
        # print(groupNumber)
        for user in manipJson:
            # print(f"user : {user}")
            # print(f"id: {user['id']}")
            # print(user)
            if groupNumber['groupID'] == user['groupID']:
                group_User.append({"Group": groupnames[index]['groupName'], "userEmail": user['email']})
        index += 1

    # print(group_User)
    return group_User


def create_moodle_groups_to_gitlab(courseID: int, parent_group_id: int):
    """
    This function retrieves the manually created groups in moodle given the courseID in parameter
    and creates them and add the users in the gitLab parent group.

    :param courseID: Moodle course ID where groups exists.
    :param parent_group_id: Gitlab existing group ID where we want to create the subgroups.
    """

    groupInfo = moodleAPI.get_group_name_by_courseid(courseID)
    retrieveGroupID = []
    for oneGroup in groupInfo:
        retrieveGroupID.append(oneGroup['groupID'])

    # retrieve list of dict with [{'groupID' : number, 'userID' : [list of all user ids]}, ...]
    group_members = moodleAPI.get_group_members_by_id(retrieveGroupID)
    # writes user email address in a json file so that it can be parsed and used by following function
    # print(groupInfo)
    write_response_json(moodleAPI.get_user_email_from_id(group_members, True), 'moodleInfo')
    # reads json file and by giving group_members (to match user emails with correct groups) creates the group and
    # adds users in them

    gitlabAPI.create_add_groups(JSON_parse_email_moodle(group_members, groupInfo, "moodleInfo"),
                                parent_group_id)


def grade_feedback_indivual_student(courseID: int, assignmentName: str, base_proj_ID: int):
    """
    This function retrieves the existing gitlab issue description and grades automatically all the moodle assignments
    existing in the given course ID, only for individual assignment.
    Grades all the students found in course, if user ID = 0 then user didn't have public email in gitlab.

    :param courseID: Moodle course ID where the assignments are present.
    :param assignmentName: List of assignment names needed to be graded, can be single one too.
    """
    courseMembers = moodleAPI.get_user_in_course(courseID)
    usersGraded = []
    submissionStatus = []
    for aStudent in courseMembers:
        if aStudent['roles'][0]['shortname'] == 'student':
            usersGraded.append(aStudent['email'])

    for oneStudent in usersGraded:
        # print(oneStudent)
        student_moodle_ID = moodleAPI.get_user_id_from_email(oneStudent)
        # gets the ids of all the assignement corresponding to the course Id in parameter
        assignment_id = moodleAPI.get_course_assignement_ID(courseID, assignmentName)
        # print(assignment_id)
        # student git id
        user_git_ID = gitlabAPI.get_userid_byUserEmail(oneStudent)
        if user_git_ID:
            fork_project_ID = gitlabAPI.get_fork_proj_ID(base_proj_ID, user_git_ID)
            # gets a dict with information about all the issues concerning the user_git_ID matching the projects IDs owned
            # by the teacher
            # print("projID :", fork_project_ID)
            if fork_project_ID:
                submissionStatus = gitlabAPI.get_issue_from_student(user_git_ID, fork_project_ID, False)
                # print("subStatus", submissionStatus)
            else:
                print("no gitlab project found for :", oneStudent)
            if submissionStatus:
                result = moodleAPI.set_assignement_grade_feedback(assignment_id, student_moodle_ID, 0, "graded", 0,
                                                                  submissionStatus)
                submissionStatus.clear()
        else:
            print("Error : user git ID is", user_git_ID, "for email", oneStudent)
        # print(result)
    # result usually null when whole system worked


def grade_feedback_multiple_students(courseID: int, assignmentName: str, parentGroupId: int):
    """
    this function retrieves gitlab issue description and grades automatically all the moodle assignement existing in
    the given course ID, only for group assignment.
    Don't care if gitlab email is private or public because issue is not for one student is from one project
    for all students found in the group

    :param courseID: Moodle course ID where the assignments are present.
    :param assignmentName: List of assignment names needed to be graded, can be single one too.
    :param groupNames: List of group names needed to be graded, can be single group too.
    :param parentGroupId: Gitlab group ID containing the subgroups with students.
    """

    # courseMembers = moodleAPI.get_user_in_course(courseID)

    groupIDs = []
    # retrieve group Name and Ids that exists in the course
    groupName = moodleAPI.get_group_name_by_courseid(courseID)
    # gather only IDs
    for aGroup in groupName:
        groupIDs.append(aGroup['groupID'])
    # retrieve group members ID in function of group ID
    groupMembersID = moodleAPI.get_group_members_by_id(groupIDs)
    # with member ID get each group Email
    memberEmailAndGroup = json.loads(moodleAPI.get_user_email_from_id(groupMembersID, True))

    # We need the dependence between group ID and corresponding member Email
    groupMembersEmail = []
    # pass through the info with userIDs and groups
    #  [{'groupID': 1, 'userID': [4, 8]}, ... ]
    for oneGroup in groupMembersID:
        groupMembersEmail.append({'groupID' : oneGroup['groupID'], 'memberEmail' : []})
        # pass through the info containing [{'userID' : X, 'userEmail' : Y, 'moodleGroupID' : Z }, ...]
        for oneStudent in memberEmailAndGroup:
            if oneGroup['groupID'] == oneStudent['groupID']:
                # make a list with only '[{'groupID': 1, 'memberEmail' : [X, Y, Z]}, ...]'
                for sameGroup in groupMembersEmail:
                    if sameGroup['groupID'] == oneStudent['groupID'] :
                        sameGroup['memberEmail'].append(oneStudent['email'])

    assignment_id = moodleAPI.get_course_assignement_ID(courseID, assignmentName)
    allSubGroupsGit = gitlabAPI.get_all_subgroup(parentGroupId)
    # print(allSubGroupsGit)
    # print(groupMembersEmail)

    index = 0
    for oneGroup in groupMembersEmail:
        # SECURITY MATCHING FOR LINK BTW GIT - MOODLE SUBGROUP NAMES AND IDS
        # NOT NECESSARY IF GROUPS ARE ALWAYS IN SAME ORDER
        for checkGroup in groupName:
            # verification that it is the same moodle group ID
            if checkGroup['groupID'] == oneGroup['groupID']:
                # iterate through git retrieved subGroups
                for gitsubGroup in allSubGroupsGit:
                    thesubGroupName = next(iter(gitsubGroup.values()))
                    # verification that the name of the two groups are the same
                    if thesubGroupName == checkGroup['groupName']:
                        # attribution of git group ID to retrieve correct issue
                        thesubGroupID = next(iter(gitsubGroup.keys()))
                        # print(thesubGroupID)

        myProjID = gitlabAPI.get_group_projects(thesubGroupID, assignmentName)

        for oneStudent in oneGroup['memberEmail']:

            user_git_ID = gitlabAPI.get_userid_byUserEmail(oneStudent)
            student_moodle_ID = moodleAPI.get_user_id_from_email(oneStudent)
            groupIssue = gitlabAPI.get_issue_from_student(user_git_ID, [myProjID], True)

            if groupIssue:
                print("Group =", groupName[index]['groupName'])
                result = moodleAPI.set_assignement_grade_feedback(assignment_id, student_moodle_ID, 0, "graded", 0,
                                                                  groupIssue)
        index += 1

    # print(result)
        # result usually null when whole system worked


def analyze_commits(projID: int, days: bool, minParticipation: float, figureName: str):
    """
    This function is used to analyse commits for a gitlab project.
    It saves two diagrams in a .png image describing the participation in function of time.
    One with the number of commits for each week, the other one with the number of line added, for each week.
    (project ID used for verification= 36486)

    :param projID: Gitlab project that needs analysis.
    :param days: Granularity variable (True = days / False = weeks).
    :param minParticipation:
    :param figureName:
    """


    # print(projID)
    allCommits, begEndDates = gitlabAPI.get_commits_from_project(projID)
    # Security check
    if allCommits == 0 or begEndDates == 0 :
            return
    beginobj = datetime.datetime.strptime(begEndDates[0], "%Y-%m-%dT%H:%M:%S.%f%z")
    endobj = datetime.datetime.strptime(begEndDates[1], "%Y-%m-%dT%H:%M:%S.%f%z")
    begin = beginobj.strftime("%d-%m-%YT%H:%M:%S.%f%z")
    end = endobj.strftime("%d-%m-%YT%H:%M:%S.%f%z")

    totalOfCommits = len(allCommits)
    print("Number of commits : ", totalOfCommits)
    projectMembers = {}
    numOfCommitsPerWeek = {}
    addPerWeek = {}
    begin_date = datetime.datetime.strptime(begin, "%d-%m-%YT%H:%M:%S.%f%z")
    end_date = datetime.datetime.strptime(end, "%d-%m-%YT%H:%M:%S.%f%z")

    if days:
        print("Granularity = days")
        # project duration in dayNumber ( + 1 because we also include the day of the begin not only how many days
        # to count from begin to arrive to end_date )
        dayNumber = round((end_date - begin_date).days) + 1
        # + 1 because example week begin = 12 week end = 22
        # week difference = 22 - 12 = 10 but in reality you count the 12th week also so 11 in total
        numberOfBuckets = list(range(dayNumber + 1))
        yearDayBegin = int(begin_date.strftime("%j"))


    else:
        print("Granularity = weeks")
        # stores weeknumber for project duration
        weekNumber = math.ceil((end_date - begin_date).days / 7)

        # + 1 because example week begin = 12 week end = 22
        # week difference = 22 - 12 = 10 but in reality you count the 12th week also so 11 in total
        numberOfBuckets = list(range(weekNumber + 1))
        yearWeekBegin = int(begin_date.strftime("%V"))

    # reverse list of all commits because initial value is from most recent commit to oldest one.
    sorted_commit_data = list(reversed(allCommits))

    projMembers = gitlabAPI.get_proj_members(projID)
    # print(projMembers)
    for oneUser in projMembers :
        numOfCommitsPerWeek[oneUser['name']] = [0] * len(numberOfBuckets)
        addPerWeek[oneUser['name']] = [0] * len(numberOfBuckets)


    for aCommit in sorted_commit_data:

        # memberEmail = aCommit['author_email']
        memberName = aCommit['author']
        # Verification that user is enlisted as project member if not just skip to next commit
        if memberName in numOfCommitsPerWeek:
            # numOfCommitsPerWeek[memberName] = [0] * len(numberOfBuckets)
            # addPerWeek[memberName] = [0] * len(numberOfBuckets)

            if days:
                myCommitDay = aCommit['Date'].strftime("%j")
                theBucketIndex = int(myCommitDay) - yearDayBegin
            else:
                myCommitWeek = aCommit['Date'].strftime("%V")
                theBucketIndex = int(myCommitWeek) - yearWeekBegin

            numOfCommitsPerWeek[memberName][theBucketIndex] += 1
            addPerWeek[memberName][theBucketIndex] += aCommit['Additions']

    result, (commitDetail, additionDetail) = plt.subplots(1, 2)

    # print(numOfCommitsPerWeek)
    # print(addPerWeek)

    for oneMember, numOfAdd in addPerWeek.items():
        additionDetail.plot(numberOfBuckets, numOfAdd, label=oneMember, marker='x')

    for oneMember, numOfCommits in numOfCommitsPerWeek.items():
        commitDetail.plot(numberOfBuckets, numOfCommits, label=oneMember, marker='x')

    if days:
        commitDetail.set_title("Number of commits/days")
        commitDetail.set_xlabel("Duration of project in days")

        additionDetail.set_title("Number of additions in line/days")
        additionDetail.set_xlabel("Duration of project in days")

    else:
        commitDetail.set_title("Number of commits/week")
        commitDetail.set_xlabel("Duration of project in week")

        additionDetail.set_title("Number of additions in line/week")
        additionDetail.set_xlabel("Duration of project in week")

    commitDetail.set_ylabel("Number of commits")
    commitDetail.legend()
    commitDetail.grid(True)

    additionDetail.set_ylabel("Number of line added")
    additionDetail.legend()
    additionDetail.grid(True)

    result.set_size_inches(18, 10)
    plt.savefig("output/commit_analysis_" + figureName + ".png")
    plt.close(result)
    # give pourcentage as ouput
    for oneCommit in allCommits:

        if not projectMembers.get(oneCommit['author_email']):
            projectMembers.update({oneCommit['author_email']: 1})
        else:
            projectMembers[oneCommit['author_email']] += 1

    # if minimum pourcentage is not set update
    if minParticipation is None:
        minimumPourcent = 100 / len(projectMembers)
    # else consider limit value in file
    else:
        minimumPourcent = minParticipation

    participationPourcent = {}

    with open('output/result.txt', 'a') as f:
        message = "Project ID = " + str(projID) + "\n"
        f.write(message)
        for oneMember, numberOfCommits in projectMembers.items():
            participationPourcent[oneMember] = (numberOfCommits / totalOfCommits) * 100
            if participationPourcent[oneMember] < minimumPourcent:
                message = "ATTENTION : " + oneMember + " is under minimum rate of participation = " + str(
                    participationPourcent[oneMember]) + " %\n"
                f.write(message)
                print(message)

        print("Participation pourcentage for each member :", participationPourcent)
        print("\n\n")
# a comment

if __name__ == '__main__':

    example_config_GT = """
    {
        "course_id" : aNumber,
        "parent_group_id" : aNumber,
        "moodleTeacherToken" : "your_moodle_token",
        "gitPAToken" : "your_gitlab_personal_access_token",
        "gitDomain" : "your-domain-for-gitlab/",
        "moodleDomain": "your-domain-for-moodle/"
    }
    """

    example_config_GS = """
        {
            "course_id" : aNumber,
            "assignmentNames": "oneAssignment",
            "moodleTeacherToken" : "your_moodle_token",
            "gitPAToken" : "your_gitlab_personal_access_token",
            "gitDomain" : "your-domain-for-gitlab/",
            "moodleDomain": "your-domain-for-moodle/"
        }
        """

    example_config_GM = """
            {
                "course_id" : aNumber,
                "assignmentNames": "oneAssignment",
                "parent_group_id": aNumber,
                "moodleTeacherToken" : "your_moodle_token",
                "gitPAToken" : "your_gitlab_personal_access_token",
                "gitDomain" : "your-domain-for-gitlab/",
                "moodleDomain": "your-domain-for-moodle/"
            }
            """

    example_config_AC = """
            {
                "projID" : aNumber,
                "moodleTeacherToken" : "your_moodle_token",
                "gitPAToken" : "your_gitlab_personal_access_token",
                "gitDomain" : "your-domain-for-gitlab/",
                "moodleDomain": "your-domain-for-moodle/"
            }
            """

    parser = argparse.ArgumentParser(description='Python script for moodle-gitlab synchronization')

    subparsers = parser.add_subparsers(dest='command')

    # Subparser for 'transf_groups' command
    parser_transf_groups = subparsers.add_parser('transf_groups', aliases=['TG'],
                                                 help='Transfer Moodle groups to GitLab.',
                                                 formatter_class=argparse.RawDescriptionHelpFormatter)
    parser_transf_groups.add_argument('config_file', help='Path to the JSON configuration file. \n')
    parser_transf_groups.description = 'Analyzes all Moodle groups present in the course, creates them in GitLab, and adds the corresponding members.'
    parser_transf_groups.epilog = "Example config_file :" + example_config_GT

    # Subparser for 'grade_single' command
    parser_grade_single = subparsers.add_parser('grade_single', aliases=['GS'], help='Grade an individual project.',
                                                formatter_class=argparse.RawDescriptionHelpFormatter)
    parser_grade_single.add_argument('config_file', help='Path to the JSON configuration file.')

    parser_grade_single.description = 'Grades an individual project for all students present in the course.'
    parser_grade_single.epilog = "Example config_file :" + example_config_GS

    # Subparser for 'grade_mul' command
    parser_grade_mul = subparsers.add_parser('grade_mul', aliases=['GM'], help='Grade a group project',
                                             formatter_class=argparse.RawDescriptionHelpFormatter)
    parser_grade_mul.add_argument('config_file', help='Path to the JSON configuration file.')
    parser_grade_mul.epilog = "Example config_file :" + example_config_GM

    parser_analyse_com = subparsers.add_parser('analyze_com', aliases=['AC'], help='Analyze project commits',
                                               formatter_class=argparse.RawDescriptionHelpFormatter)
    parser_analyse_com.add_argument('config_file', help='Path to the JSON configuration file. \n')
    parser_analyse_com.description = 'Analyzes participation of members in project given in parameter, based on commits'
    parser_analyse_com.epilog = "Example config_file :" + example_config_AC

    args = parser.parse_args(sys.argv[1:])

    config = open(args.config_file)
    myfile = json.loads(config.read())

    gitlabAPI.myToken2 = myfile['gitPAToken']
    gitlabAPI.headers = {"PRIVATE-TOKEN": gitlabAPI.myToken2}

    gitlabAPI.gitDomain = myfile['gitDomain']
    gitlabAPI.base_url = "https://" + gitlabAPI.gitDomain + "api/v4/"
    gitlabAPI.group_base_url = gitlabAPI.base_url + "groups/"
    gitlabAPI.proj_base_url = gitlabAPI.base_url + "projects/"
    # print(gitlabAPI.base_url)


    # print("git token", gitlabAPI.myToken2)
    # print("moodle token", moodleAPI.usedToken)

    if args.command == "transf_groups" or args.command == "TG":
        print("transfer moodle group to gitlab")

        moodleAPI.usedToken = myfile['moodleTeacherToken']
        moodleAPI.moodleDomain = myfile['moodleDomain']
        moodleAPI.base_url = "https://" + moodleAPI.moodleDomain + "webservice/rest/server.php?"
        course_id = myfile['course_id']
        parent_group_id = myfile["parent_group_id"]
        create_moodle_groups_to_gitlab(course_id, parent_group_id)

    elif args.command == "grade_single" or args.command == "GS":
        print("grade single students")

        moodleAPI.usedToken = myfile['moodleTeacherToken']
        moodleAPI.moodleDomain = myfile['moodleDomain']
        moodleAPI.base_url = "https://" + moodleAPI.moodleDomain + "webservice/rest/server.php?"
        course_id = myfile['course_id']
        assignmentNames = myfile['assignmentNames']
        base_proj_ID = myfile['base_proj_ID']
        grade_feedback_indivual_student(course_id, assignmentNames, base_proj_ID)

    elif args.command == "grade_mul" or args.command == "GM":
        print("grade multiple students")

        moodleAPI.usedToken = myfile['moodleTeacherToken']
        moodleAPI.moodleDomain = myfile['moodleDomain']
        moodleAPI.base_url = "https://" + moodleAPI.moodleDomain + "webservice/rest/server.php?"
        course_id = myfile['course_id']
        assignmentNames = myfile['assignmentNames']
        parent_group_id = myfile['parent_group_id']
        grade_feedback_multiple_students(course_id, assignmentNames, parent_group_id)

    elif args.command == "analyze_com" or args.command == "AC":
        print("commit analysis")
        projectName = myfile['project_name']
        granularity = myfile['granularity']

        # parsing granularity param
        if granularity == "days":
            days = True
        elif granularity == "weeks":
            days = False
        else:
            print("Granularity is not a value.\nPlease enter : 'days' or 'weeks'")
            exit()

        if "minParticipation" in myfile:
            minParticipation = myfile['minParticipation']
        else:
            minParticipation = None

        if "parentGroupId" in myfile:
            parentGroupId = myfile['parentGroupId']
        else :
            parentGroupId = None
        # need to retrieve the project ID with correct name of all subgroups in parentGroup

        if parentGroupId:
            allSubGroups = gitlabAPI.get_all_subgroup(parentGroupId)
            # each project ID that needs analysis
            for aSubgroup in allSubGroups:

                subGroupID = aSubgroup.keys()
                subGroupName = aSubgroup.values()
                theID = next(iter(subGroupID))
                theName = next(iter(subGroupName))

                print("\nAnalysing group :", theName)

                projID = gitlabAPI.get_project_ID_in_subGroup(theID, projectName)
                if projID:
                    analyze_commits(projID, days, minParticipation, theName)
                else:
                    print("project '", projectName, "' does not exist in subgroup :", theName)
        else:

            allProjects = gitlabAPI.get_all_project_IDs_matching_name(projectName)

            if allProjects:
                index = 0
                for oneProject in allProjects:
                    figureName = "projID_" + str(oneProject)
                    analyze_commits(oneProject, days, minParticipation, figureName)
                    index += 1


            else:
                print("Not a single project found matching the name given")
                assert()
        subprocess.run(["jupyter", "nbconvert", "--to", "notebook", "--execute", "plots_notebook.ipynb"])
